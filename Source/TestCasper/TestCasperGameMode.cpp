// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TestCasper.h"
#include "TestCasperGameMode.h"
#include "TestCasperCharacter.h"

ATestCasperGameMode::ATestCasperGameMode()
{
	// set default pawn class to our character
	DefaultPawnClass = ATestCasperCharacter::StaticClass();	
}
